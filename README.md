Stereo player using two rtsp h264 streams for Ubuntu
====================================================

Authors : Brian Soe  
Contact : bsoe at stanford.edu

About
-----

Uses Opencv to stream from two seperate rtsp h264 streams. The windows are placed side by side to mimic the side by side format of 3D video. Use keyboard to adjust window position to align the two image streams for stereo vision.  

The code was developed for Ubuntu, using [Point Grey Cricket IP Camera]. See [Cricket Setup.md] for instructions on minimizing latency.


Installation
------------

1. Download the respository

        cd Documents
        git clone https://bsoe@bitbucket.org/bsoe/stereo-player.git

2. Download the latest [opencv], which contains bug fixes for rtsp streaming

        git clone https://github.com/Itseez/opencv.git ~/lib/opencv

3. Install dependencies

        sudo apt-get install build-essential
        sudo apt-get install cmake git libgtk2.0-dev pkg-config libavcodec-dev libavformat-dev libswscale-dev
        sudo apt-get install freeglut3 freeglut3-dev

4. Install the library locally

        cd ~/lib/opencv
        mkdir build
        cd build
        cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=~/lib/opencv/libopencv ..
        make -j3
        make install

5. Make link to opencv in stereo-player folder

        cd Documents/stereo-player/
        ln -s ~/lib/opencv/libopencv opencv

6. Compile and run

        cd Documents/stereo-player
        mkdir build
        cd build
        cmake ..
        make
        ./stereo-player

[opencv]:https://github.com/Itseez/opencv
[Point Grey Cricket IP Camera]:http://www.ptgrey.com/cricket-ip-security-camera
[Cricket Setup.md]:https://bitbucket.org/bsoe/stereo-player/src/master/cricket_setup/SETUP.md