***Cricket IP Camera Setup***
=============================

Table of Contents
-----------------

- Summary
- Documents
- Connecting to Camera
- Minimizing Latency
- Assigning Static IP Address

Summary
-------
This document explains how to setup the Cricket IP Camera,  
to live stream using VLC, with minimal lag.

Documents
---------
[Website]  
[Datasheet] 

Connecting to Camera
--------------------

1. Plug in cameras, to the computer, and to a POE power adaptor
2. On a Windows computer, download and install [ONVIF Device Manager]
3. Refresh ONVIF, all cameras should be listed with IP address
4. Open up camera in browser to IP address: https://X.X.X.X
5. login with user: root / password: password 

Streaming in VLC
----------------
A broadcast url will be listed, that can be opened in VLC  
vlc->media ->open network stream  
Use advanced options to set buffer, ie video latency.  
(250 ms for default settings, 100 ms for minimized latency settings)

Minimizing Latency
------------------
The latency can be reduced to 100-400 ms, using the following settings, 
shown in detail in the images below.  

- Increase the frame rate to maximum (60FPS)
- Increase the compression quality (99 for MJPEG / 32000000 for H.264)
- Turn off Noise Reduction (Both NR-3D and NR-2D)
- Turn off Extended Shutter

![helloworld](Sensor_Mode.png "Sensor Mode")  
![helloworld](Streams.png "Streams")  
![helloworld](Image_Appearance.png "Image Appearance")

Assigning Static IP Address
---------------------------
By default, the camera IP are set dynamically.  
However, we have setup the following static IP address for the two underwater robot cricket.
192.168.1.100
192.168.1.101

To assign a static IP address:

1. In the browser, navigate to the camera ip address for camera settings  
2. Switch to the network page  
3. Use the following settings, where X is large, but less than 255.  

        IP: 192.168.1.X  
        Submask: 255.255.0.0  
        Gateway: Blank

4. Set the compute ip address. Open up network connections and add new connection.
5. In the _Ethernet_ tab, choose desired ethernet card mac address, and set _MTU_ to _9000_
6. In the _IPV4 Settings_ tab, select method as _Manual_. Set the IP address, where Y is neither 0, 255, nor X.

        address 192.168.1.Y
        netmask 255.255.0.0  




[Website]: http://www.ptgrey.com/cricket-ip-security-camera
[Datasheet]:https://www.ptgrey.com/support/downloads/10302/
[ONVIF Device Manager]:http://sourceforge.net/projects/onvifdm/
