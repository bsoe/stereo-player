#include "StereoPlayer.h"

#include <unistd.h>

#ifdef STEREOPLAYER_USE_RUIN

#include <ruin/Object.h>
#include <ruin/MsgEncoder.h>
#include <ruin/ObjectHandler.h>

// redis object for stereo stream
class StereoPlayerObject : public ruin::Object, public StereoPlayer
{
public:
    StereoPlayerObject() : ruin::Object("CAMERA", "stereo"){
        ruinAddParameter(restart_str);
        ruinAddParameter(x_str);
        ruinAddParameter(y_str);
        ruinAddParameter(crop_str);

	ruinSetParameter(x_str,"20");
	ruinSetParameter(y_str,"-10");
    ruinSetParameter(crop_str, "20");
    }

    virtual std::string ruinParameter(const std::string& name){
        if (name==restart_str) {
            return ruin::encode(restart);
        }
        else if (name==x_str) {
            return ruin::encode(x);
        }
        else if (name==y_str) {
            return ruin::encode(y);
        }
        else if (name==crop_str) {
            return ruin::encode(crop);
        }
        else if (name == delay_str) {
            return ruin::encode(capture_delay);
        }
        else {std::cout << "unkown param: " << name << std::endl;}
        return "";
    }

    virtual void ruinSetParameter(const std::string& name, const std::string& value){
        if (name==restart_str){
            ruin::decode(value, restart);
        }
        else if (name==x_str){
            ruin::decode(value, x);
            left_offset_x = -x;
            right_offset_x = x;
            moveWindow();
        }
        else if (name==y_str){
            ruin::decode(value, y);
            left_offset_y = -y;
            right_offset_y = y;
            moveWindow();
        }
        else if (name==crop_str){
            ruin::decode(value, crop);
            left_crop = crop;
            right_crop = crop;
            moveWindow();
        }
        else if (name==delay_str){
            ruin::decode(value, capture_delay);
        }
        else {std::cout << "unkown param: " << name << std::endl;}
    }

    bool restart = true;    const std::string restart_str = "restart";
    int x = 0;              const std::string x_str = "x";
    int y = 0;              const std::string y_str = "y";
    int crop = 0;           const std::string crop_str = "crop";
                            const std::string delay_str = "delay";

};

int main(int, char**)
{
    // create stereo stream
    StereoPlayerObject stereo_player;

    // create redis objects
    ruin::ObjectHandler handler;
    handler.addObject(&stereo_player);
    handler.connect();
    handler.pushObjects();

    // start restart loop
    while (stereo_player.restart == true)
    {
        stereo_player.start();

        stereo_player.restart = false;
        stereo_player.ruinUpdateParameter(stereo_player.restart_str);

        // continually check for restart signal
        while(stereo_player.isRunning())
        {
            handler.pullObjectUpdates();
            handler.pushObjectUpdates();

            if (stereo_player.restart == true){
                stereo_player.stop();
            }

            usleep(100000); //useconds
       }
    }

    stereo_player.stop();

    return 0;
}

#else

int main(int, char**)
{
    StereoPlayer stereo_player;

    stereo_player.left_offset_x = -20;
    stereo_player.right_offset_x = 20;

    stereo_player.left_offset_y = 10;
    stereo_player.right_offset_y = -10;

    stereo_player.left_crop = 20;
    stereo_player.right_crop = 20;

    // start restart loop
    while (true)
    {
        stereo_player.start();

        while(stereo_player.isRunning())
        {
            if (stereo_player.lastKeyPressed() == 'r'){
                std::cout << "should reset" << std::endl;
                stereo_player.stop();
            }
        }
        usleep(100000); //useconds
        if (stereo_player.lastKeyPressed() == 27){
            break;
        }
        stereo_player.clearLastKeyPressed();
    }
    
    return 0;
}

#endif

