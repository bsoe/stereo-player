CMAKE_MINIMUM_REQUIRED(VERSION 2.8)

project(stereo-player)

############## CMAKE OPTIONS #####################

set(CMAKE_VERBOSE_MAKEFILE ON)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -std=c++0x")
set(CMAKE_BUILD_TYPE Debug)

############### SOURCE FILES ######################

set(SOURCES main.cpp
            StereoPlayer.cpp
)

add_executable( ${PROJECT_NAME} ${SOURCES} 
)


############### MODULES ######################

target_link_libraries(${PROJECT_NAME} glut GLU GL -lX11)

set(CMAKE_PREFIX_PATH ${CMAKE_CURRENT_SOURCE_DIR}/opencv)
find_package(OpenCV REQUIRED)
include_directories(${OpenCV_INCLUDE_DIRS})
target_link_libraries(${PROJECT_NAME} ${OpenCV_LIBS})
