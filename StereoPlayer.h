#ifndef StereoPlayer_H
#define StereoPlayer_H

#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/calib3d/calib3d.hpp>

#include <iostream>
#include <string>

#include <thread>
#include <mutex>

class StereoPlayer
{
public:
    StereoPlayer();
    virtual ~StereoPlayer();

    /** start the streams. nonblocking. */
    void start();

    /** start the streams. nonblacking.
     *  specifying screen size if multiple monitors. */
    void start(int screen_width, int screen_height);

    /** waits for escape. blocking */
    void waitForEscape();

    /** force streams to stop */
    void stop();

    /** moves left window based on left_offset */
    void moveLeftWindow();

    /** moves right window based on right_offset */
    void moveRightWindow();

    /** returns true if video capture is running */
    bool isRunning(){return running;}

    /** returns true if video is frozen on either camera */
    bool isFrozen(){return frozen;}

    /** returns last key press */
    char lastKeyPressed(){return last_key;}

    /** set the last key back to default value of 0 */
    char clearLastKeyPressed(){last_key = 0;}

    /** read the camera parameters for the files **/

    unsigned int capture_delay = 2; //ms

    std::string left_stream_address = "rtsp://192.168.168.100/unicast/h264:video_stream_1.ini";
    std::string right_stream_address = "rtsp://192.168.168.101/unicast/h264:video_stream_1.ini";

    std::string left_parameters_file = "../left_camera_parameters.xml";
    std::string right_parameters_file = "../right_camera_parameters.xml";

    cv::Mat left_map1, left_map2, right_map1, right_map2;

    bool undistort_images = false;

    unsigned long long grab_left = 0;
    unsigned long long grab_right = 0;

    int left_offset_x = 0; // shift left/right x pixels (+ is right)
    int left_offset_y = 0; // shift up/down y pixels (+ is down)

    int right_offset_x = 0; // shift left/right x pixels (+ is right)
    int right_offset_y = 0; // shift up/down y pixels (+ is down)

    unsigned int left_crop = 0; // crop pixels off left side of left image
    unsigned int right_crop = 0; // crop pixels off right side of right image

    // ================================
    // INTERNAL FUNCTIONS - DO NOT USE
    // ================================
    void leftCapture();

    void rightCapture();

    static void *leftCaptureHelper(void *context){
        ((StereoPlayer *)context)->leftCapture();
    }

    static void *rightCaptureHelper(void *context){
        ((StereoPlayer *)context)->rightCapture();
    }

private:

    void loopWait();

    // ================================
    // USER PARAMETERS
    // ================================

    const double SCALING = 0.5; //without opengl, anything except 0.5 is very slow

    const int THREAD_PRIORITY = -18; // highest priority is -20, lowest priority is 20

    const std::string LEFT_WINDOW_NAME = "left_eye";
    const std::string RIGHT_WINDOW_NAME = "right_eye";

    // ================================
    // DATA
    // ================================

    cv::VideoCapture video_left;
    cv::VideoCapture video_right;
    cv::Mat frame_left;
    cv::Mat frame_right;

    cv::Mat frame_stretched_left;
    cv::Mat frame_stretched_right;

    cv::Mat frame_cropped_left;
    cv::Mat frame_cropped_right;

    cv::Mat last_frame_left;
    cv::Mat last_frame_right;

    int screen_w = 0;
    int screen_h = 0;

    int stretch_rows = 0;
    int stretch_cols = 0;
    cv::Size stretch_size;

    std::thread* thread_left = NULL;
    std::thread* thread_right = NULL;
    std::mutex lock_wait_key;
    std::mutex lock_capture;
    std::mutex lock_imshow;


    unsigned long counter_left = 0;
    unsigned long counter_right = 0;

    bool running = false;
    bool frozen = false;

    bool move_left = false;
    bool move_right = false;
    bool capture_left = false;
    bool capture_right = false;

    unsigned int capture_number = 0;

    char last_key = 0;
};

#endif // StereoPlayer_H
