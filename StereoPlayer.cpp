#include "StereoPlayer.h"

// image processing
#include <opencv2/imgproc/imgproc.hpp>

// getting screen size
#include <X11/Xlib.h>
#include <stdio.h>

// setting thread priority
#include <unistd.h>
#include <sys/resource.h>

using namespace std;

StereoPlayer::StereoPlayer()
{
}

StereoPlayer::~StereoPlayer()
{
    delete thread_left;
    delete thread_right;
}

void StereoPlayer::start()
{
    start(0,0);
}


void StereoPlayer::start(int screen_width, int screen_height)
{
    std::cout << "intiializing stereo stream" << std::endl;

    stop();

    video_left = cv::VideoCapture();
    if(!video_left.open(left_stream_address)) {
        cout << "Error opening video stream or file" << endl;
        return;
    }

    video_right = cv::VideoCapture();
    if(!video_right.open(right_stream_address)) {
        cout << "Error opening video stream or file" << endl;
        return;
    }

    // grab one frame for size
    while(1) {
        bool success = video_left.read(frame_left);
        if (success){ break; }
    }
    while(1) {
        bool success = video_right.read(frame_right);
        if (success){ break; }
    }

    stretch_rows = 2*SCALING*frame_left.rows;
    stretch_cols = SCALING*frame_left.cols;
    stretch_size = cv::Size(stretch_cols,stretch_rows);
    frame_stretched_left = cv::Mat(stretch_rows, stretch_cols, frame_left.type());
    frame_stretched_right = cv::Mat(stretch_rows, stretch_cols, frame_left.type());
    std::cout << "frame: " << stretch_cols << " X " << stretch_rows << std::endl;

    cv::Rect roi_left(left_crop,0,stretch_cols-left_crop,stretch_rows);
    cv::Rect roi_right(0,0,stretch_cols-right_crop,stretch_rows);
    frame_cropped_left = frame_stretched_left(roi_left);
    frame_cropped_right = frame_stretched_right(roi_right);

    last_frame_left = frame_cropped_left.clone();
    last_frame_right = frame_cropped_right.clone();

    // create windows
    cv::namedWindow(LEFT_WINDOW_NAME, cv::WINDOW_AUTOSIZE);
    cv::namedWindow(RIGHT_WINDOW_NAME, cv::WINDOW_AUTOSIZE);
    cv::imshow(LEFT_WINDOW_NAME, frame_cropped_left);
    cv::imshow(RIGHT_WINDOW_NAME, frame_cropped_right);

    // obtain screen resolution
    if (screen_width==0 || screen_height == 0){
        Display* disp = XOpenDisplay(NULL);
        Screen*  scrn = DefaultScreenOfDisplay(disp);
        //Screen* scrn = XScreenOfDisplay(disp, 0);
        screen_w  = scrn->width;
        screen_h = scrn->height;
        std::cout << "screen count: " << XScreenCount(disp) << std::endl;
        std::cout << "screen: " << scrn->width << " X " << scrn->height << std::endl;
         XCloseDisplay( disp );
    } else {
        screen_w = screen_width;
        screen_h = screen_height;
    }

    moveLeftWindow();
    moveRightWindow();

    std::cout << "========================\n";
    std::cout << "exit             : esc\n";
    std::cout << "move left  window: asdw\n";
    std::cout << "move right window: jkli\n";
    std::cout << "capture image    : space\n";
    std::cout << "========================\n";

    std::cout << "Beginning capture loop." << std::endl;

    delete thread_left;
    delete thread_right;  
    
    if(undistort_images)
    {
        cv::FileStorage fsl(left_parameters_file, cv::FileStorage::READ);
        cv::FileStorage fsr(right_parameters_file, cv::FileStorage::READ);
        cout << left_parameters_file << endl;

        std::string datel, dater;
        fsl["calibration_Time"] >> datel;
        fsr["calibration_Time"] >> dater;

        cout << "date right calibration : " << dater << endl
             << "date left calibration : " << datel << endl;
             
        cv::Mat left_cameraMatrix, left_distCoeffs;
        cv::Mat right_cameraMatrix, right_distCoeffs;
        fsl["Camera_Matrix"] >> left_cameraMatrix;
        fsl["Distortion_Coefficients"] >> left_distCoeffs;
        fsr["Camera_Matrix"] >> right_cameraMatrix;
        fsr["Distortion_Coefficients"] >> right_distCoeffs;

        fsl.release();
        fsr.release();

        cv::initUndistortRectifyMap(left_cameraMatrix, left_distCoeffs, cv::Mat(), left_cameraMatrix, frame_left.size(), CV_32FC1, left_map1, left_map2);
        cv::initUndistortRectifyMap(right_cameraMatrix, right_distCoeffs, cv::Mat(), right_cameraMatrix, frame_right.size(), CV_32FC1, right_map1, right_map2);
    } 

    running = true;
    thread_left = new std::thread(rightCaptureHelper,this);
    thread_right = new std::thread(leftCaptureHelper,this);
}

void StereoPlayer::stop()
{
    running = false;

    waitForEscape();

    video_left.release();
    video_right.release();

    cv::destroyAllWindows();
}


void StereoPlayer::waitForEscape()
{
    if (thread_left!=NULL){
        thread_left->join();
        thread_left = NULL;
    }
    if (thread_right!=NULL){
        thread_right->join();
        thread_right = NULL;
    }
}


void StereoPlayer::leftCapture()
{
    pid_t pid = getpid();
    int priority_status = setpriority(PRIO_PROCESS, pid, THREAD_PRIORITY);
    if (priority_status){
        std::cout << "Failed to set high thread priority. Did you run as sudo?" << std::endl;
    }

    while(isRunning()){
        if(!video_left.read(frame_left)) {
            cout << "No frame" << endl;
        }

        if(grab_left%4 == 0){
            if(undistort_images)
            {
                cv::Mat distorded_frame_left = frame_left.clone();
                cv::remap(distorded_frame_left, frame_left, left_map1, left_map2, cv::INTER_NEAREST);
            }
            cv::resize(frame_left, frame_stretched_left, stretch_size);

            cv::Rect roi_left(left_crop,0,stretch_cols-left_crop,stretch_rows);
            frame_cropped_left = frame_stretched_left(roi_left);

            lock_imshow.lock();
            cv::imshow(LEFT_WINDOW_NAME, frame_cropped_left);
            lock_imshow.unlock();

            if (counter_left%100 == 0){
                if (std::equal(frame_cropped_left.begin<uchar>(),
                               frame_cropped_left.end<uchar>(),
                               last_frame_left.begin<uchar>())){
                    std::cout << "left window frozen." << std::endl;
                    frozen = true;
                }
                frame_cropped_left.copyTo(last_frame_left);
            }
            ++counter_left;

            loopWait();
            if (move_left){
                moveLeftWindow();
                move_left = false;
            }
            if (capture_left) {
                std::string name = "./pictures/frame_";
                name += std::to_string(capture_number);
                name += "_left";
                name += ".png";
                std::cout << name << std::endl;
                imwrite( name, frame_cropped_left );
                capture_left = false;
            }
        }
        grab_left++;
    }
}

void StereoPlayer::rightCapture()
{
    pid_t pid = getpid();
    int priority_status = setpriority(PRIO_PROCESS, pid, THREAD_PRIORITY);
    if (priority_status){
        std::cout << "Failed to set high thread priority. Did you run as sudo?" << std::endl;
    }

    while(isRunning()){
        if(!video_right.read(frame_right)) {
            cout << "No frame" << endl;
        }

        if(grab_right%4 == 0){
            if(undistort_images)
            {
                cv::Mat distorded_frame_right = frame_right.clone();
                cv::remap(distorded_frame_right, frame_right, right_map1, right_map2, cv::INTER_NEAREST);
            }

            cv::resize(frame_right, frame_stretched_right, stretch_size);

            cv::Rect roi_right(0,0,stretch_cols-right_crop,stretch_rows);
            frame_cropped_right = frame_stretched_right(roi_right);

            lock_imshow.lock();
            cv::imshow(RIGHT_WINDOW_NAME, frame_cropped_right);
            lock_imshow.unlock();

            if (counter_right%100 == 0){
                if (std::equal(frame_cropped_right.begin<uchar>(),
                               frame_cropped_right.end<uchar>(),
                               last_frame_right.begin<uchar>())){
                    std::cout << "right window frozen." << std::endl;
                    frozen = true;
                }
                frame_cropped_right.copyTo(last_frame_right);
            }
            ++counter_right;

            loopWait();
            if (move_right){
                moveRightWindow();
                move_right = false;
            }
            if (capture_right) {
                std::string name = "./pictures/frame_";
                name += std::to_string(capture_number);
                name += "_right";
                name += ".png";
                std::cout << name << std::endl;
                imwrite( name, frame_cropped_right );
                capture_right = false;
            }
        }
        grab_right++;
    }
}


void StereoPlayer::moveLeftWindow()
{
    // move x to correct position
    int x_left = screen_w/4 - stretch_cols/2 + left_offset_x + left_crop;
    int y_left = screen_h/2 - stretch_rows/2 + left_offset_y;
    //std::cout << "(" << x_left << ", " << y_left << ")" << std::endl;
    cv::moveWindow(LEFT_WINDOW_NAME, x_left, y_left);
}

void StereoPlayer::moveRightWindow()
{
    // move y to correct position
    int x_right = screen_w*3/4 - stretch_cols/2 + right_offset_x;
    int y_right = screen_h/2   - stretch_rows/2 + right_offset_y;
    //std::cout << "(" << x_right << ", " << y_right << ")" << std::endl;
    cv::moveWindow(RIGHT_WINDOW_NAME, x_right, y_right);
}



void StereoPlayer::loopWait()
{
    lock_wait_key.lock();

    char key = cv::waitKey(capture_delay);

    if (key!=-1){
        last_key = key;

        switch (key){
        case 'a':
            left_offset_x-=1; move_left=true; break;
        case 'd':
            left_offset_x+=1; move_left=true; break;
        case 'w':
            left_offset_y-=1; move_left=true; break;
        case 's':
            left_offset_y+=1; move_left=true; break;

        case 'j':
            right_offset_x-=1; move_right=true; break;
        case 'l':
            right_offset_x+=1; move_right=true; break;
        case 'i':
            right_offset_y-=1; move_right=true; break;
        case 'k':
            right_offset_y+=1; move_right=true; break;

        case 32:
            system("mkdir -p ./pictures");
            capture_number++;
            capture_left=true;
            capture_right=true;
            break;

        case 27:
            std::cout << "exiting" << std::endl;
            running = false;
        }
    }

    lock_wait_key.unlock();
}




