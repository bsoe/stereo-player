#include "StereoPlayer.h"

#include <unistd.h>

int main(int, char**)
{
    StereoPlayer stereo_player;

    stereo_player.left_stream_address = "rtsp://192.168.1.100/unicast/h264:video_stream_1.ini";
    stereo_player.right_stream_address = "rtsp://192.168.1.101/unicast/h264:video_stream_1.ini";

    stereo_player.left_offset_x = -20;
    stereo_player.right_offset_x = 20;

    stereo_player.left_offset_y = 10;
    stereo_player.right_offset_y = -10;

    stereo_player.left_crop = 20;
    stereo_player.right_crop = 20;

   stereo_player.undistort_images = true;

    if(stereo_player.undistort_images)
    {
        printf("Image correction ON\n");
    }
    else
    {
        printf("Image correction OFF\n");
    }

    // start restart loop
    while (true)
    {
        stereo_player.start();

        std::cout << "========================\n";
        std::cout << "reset player     : r\n";
        std::cout << "========================\n";

        while(stereo_player.isRunning())
        {
            if (stereo_player.lastKeyPressed() == 'r'){
                std::cout << "should reset" << std::endl;
                stereo_player.stop();
            }
        }
        if (stereo_player.lastKeyPressed() == 27){
            break;
        }
        stereo_player.clearLastKeyPressed();
        usleep(1000000); //useconds
    }
    
    return 0;
}
