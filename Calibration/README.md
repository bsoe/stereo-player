**Calibration of the two rtsp h264 cameras**

# About

Uses opencv to calibrate the cameras and store the parameters in .xml files

# Usage
### Compile the code
```sh
mkdir build
cmake ..
make
```

### Run the calibration
We calibrate one camera at the time
to run the program for the left camera type
```sh
./calibration ../left_camera_config.xml
```

to run for the right camera type
```sh
./calibration ../right_camera_config.xml
```

there is latency because only one out of 25 images is taken into account
once the camera is fixed, grab your chessboard grid, predd g and move the chessboard grig to get images. As long as we are recording images for calibration, the colors are changed. If the calibration did not succeed, it begins again untill it succeeded. Once it succedded, the reprojection error is displayed on the terminal, and the displayed view is corrected. if you are not satisfied, you can run a new calibration pressing the g key. Otherwise, just press escape to quit the program.

The created parameter files called left_camera_parameters.xml and right_camera_parameters.xml have to be placed in the root filder of the project to be read by the main stream program

```sh
cp right_camera_parameters.xml ../..
cp left_camera_parameters.xml ../.. 
```

